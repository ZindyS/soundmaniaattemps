package com.example.soundmaniatry.ui

import android.content.Context
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.soundmaniatry.R
import com.example.soundmaniatry.RecyclerAdapter
import com.example.soundmaniatry.common.Utils
import com.example.soundmaniatry.databinding.FragmentHomeBinding
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val file = Utils().getFile()
        file.mkdir()
        if (file.exists()) { fList=file.listFiles().toMutableList() }
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        root = binding.root
        root.recyclerView.adapter = RecyclerAdapter(fList, requireContext())

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onResume() {
        super.onResume()
        smt()
    }

    fun smt() {
        val handler = Handler()
        Thread {
            val runnable = object : Runnable {
                override fun run() {
                    Log.d("errror", "smt")
                    Utils().getRetrofit().getList("mp3").enqueue(
                        object : Callback<List<String>> {
                            override fun onResponse(call: Call<List<String>>, response: Response<List<String>>) {
                                if (response.isSuccessful) {
                                    check(response.body()!!)
                                } else {
                                    Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show()
                                }
                            }

                            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                                Toast.makeText(context, t.message, Toast.LENGTH_SHORT).show()
                            }
                        })
                    handler.postDelayed(this, 1000)
                }
            }
            handler.post(runnable)
        }.start()
    }

    fun check(list: List<String>) {
        for (i in list) {
            var b = false
            for (j in fList) {
                if (i==j.name) { b=true }
            }
            if (!b) {
                Utils().getRetrofit().getFile(i).enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>
                    ) {
                        if (response.isSuccessful && response.body() != null) {
                            val newFile = File(Utils().getFile(), i)
                            val render = ByteArray(4096)
                            val inputStream = response.body()!!.byteStream()
                            val outputStream = FileOutputStream(newFile)
                            while (true) {
                                val read = inputStream.read(render)
                                if (read==-1) { break }
                                outputStream.write(render, 0, read)
                            }
                            outputStream.flush()
                            fList.add(newFile)
                        } else {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        Toast.makeText(context, t.message, Toast.LENGTH_SHORT).show()
                    }
                })
                root.recyclerView.adapter = RecyclerAdapter(fList, requireContext())
                root.recyclerView.adapter!!.notifyDataSetChanged()
            }
        }
    }

    companion object {
        var ispause=false
        var fList = mutableListOf<File>()
        var mediaPlayer : MediaPlayer? = null
        lateinit var root : View
        fun startMusic(file: File, context: Context) {
            Log.d("errror", file.name)
            root.imageView5.setImageResource(R.drawable.ic_pause)
            if (mediaPlayer!= null) {
                mediaPlayer!!.stop()
                root.player.visibility = View.GONE
                mediaPlayer = null
            }
            mediaPlayer = MediaPlayer.create(context, Uri.fromFile(file))
            try {
                mediaPlayer!!.setOnPreparedListener {
                    mediaPlayer!!.setAudioAttributes(
                        AudioAttributes.Builder()
                            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                            .setUsage(AudioAttributes.USAGE_MEDIA)
                            .build()
                    )
                }
                mediaPlayer!!.start()
                root.player.visibility = View.VISIBLE
                root.name.text = file.name
                root.timeend.text = SimpleDateFormat("00:mm:ss").format(Date(mediaPlayer!!.duration.toLong()))
                root.seekBar.progress=0
                root.seekBar.max = mediaPlayer!!.duration
                var cnt : Long = 0
                val handler = Handler()
                Thread {
                    val runnable = object : Runnable {
                        override fun run() {
                            if (com.example.soundmaniatry.ui.HomeFragment.Companion.mediaPlayer!!.isPlaying) {
                                com.example.soundmaniatry.ui.HomeFragment.Companion.root.timestart.text =
                                    java.text.SimpleDateFormat(
                                        "00:mm:ss"
                                    ).format(java.util.Date(cnt * 1000))
                                com.example.soundmaniatry.ui.HomeFragment.Companion.root.seekBar.progress += 1
                                cnt++
                            }
                            handler.postDelayed(this, 1000)
                        }
                    }
                    handler.post(runnable)
                }.start()
                root.imageView5.setOnClickListener {
                    if (ispause) {
                        mediaPlayer!!.start()
                        root.imageView5.setImageResource(R.drawable.ic_pause)
                        ispause=false
                    } else {
                        mediaPlayer!!.pause()
                        root.imageView5.setImageResource(R.drawable.ic_play)
                        ispause=true
                    }
                }
            } catch (e : Exception) {
                Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
            }
        }

        fun deleteMusic(file: File, context: Context, view: View) {
            val menu = PopupMenu(context, view)
            menu.inflate(R.menu.menu)
            menu.setOnMenuItemClickListener {
                if (it.itemId==R.id.delete) {
                    Utils().getRetrofit().deleteFile(file.name).enqueue(object : Callback<ResponseBody> {
                        override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                            if (!response.isSuccessful) {
                                Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show()
                            } else {
                                fList.remove(file)
                                root.recyclerView.adapter = RecyclerAdapter(fList, context)
                                root.recyclerView.adapter!!.notifyDataSetChanged()
                            }
                        }

                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                            Toast.makeText(context, t.message, Toast.LENGTH_SHORT).show()
                        }
                    })
                }
                return@setOnMenuItemClickListener false
            }
            menu.show()
        }
    }
}