package com.example.soundmaniatry.ui

import android.annotation.SuppressLint
import android.media.MediaRecorder
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.soundmaniatry.R
import com.example.soundmaniatry.common.Utils
import com.example.soundmaniatry.databinding.FragmentDashboardBinding
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_dashboard.view.*
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.net.URLEncoder

class DashboardFragment : Fragment() {

    private var _binding: FragmentDashboardBinding? = null
    var mediaRecorder: MediaRecorder? = null
    lateinit var newFile : File

    var isRec = false
    private val binding get() = _binding!!

    @SuppressLint("NewApi")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root

        root.imageView2.setOnClickListener {
            if (root.editTextTextPersonName.text.toString()!="") {
                if (isRec) {
                    root.imageView2.setImageResource(R.drawable.ic_play_record)
                    root.chrono.base = SystemClock.elapsedRealtime()
                    root.chrono.stop()
                    mediaRecorder!!.stop()
                    mediaRecorder!!.release()
                    isRec=false

                    val map : HashMap<String, RequestBody> = HashMap()
                    val fileBody : RequestBody = RequestBody.create(MediaType.parse("audio/mp3"), newFile)
                    map["file\"; filename=\"${URLEncoder.encode(newFile.name, "utf-8")}\""] = fileBody
                    Utils().getRetrofit().saveFile(root.editTextTextPersonName.text.toString()+".mp3", map).enqueue(object :
                        Callback<ResponseBody> {
                        override fun onResponse(
                            call: Call<ResponseBody>,
                            response: Response<ResponseBody>
                        ) {
                            if (!response.isSuccessful) {
                                Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show()
                            } else {
                                Toast.makeText(context, "Stop recording", Toast.LENGTH_SHORT).show()
                            }
                        }

                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                            Toast.makeText(context, t.message, Toast.LENGTH_SHORT).show()
                        }
                    })
                    root.editTextTextPersonName.setText("")
                } else {
                    newFile = File(Utils().getFile(), root.editTextTextPersonName.text.toString()+".mp3")
                    if (newFile.exists()) {
                        Snackbar.make(root, "Файл с таким названием уже существует!", Snackbar.LENGTH_SHORT).show()
                    } else {
                        root.imageView2.setImageResource(R.drawable.ic_pause_record)
                        root.chrono.base = SystemClock.elapsedRealtime()
                        root.chrono.start()
                        mediaRecorder = MediaRecorder()
                        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
                        mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                        mediaRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
                        mediaRecorder!!.setOutputFile(newFile)
                        mediaRecorder!!.prepare()
                        mediaRecorder!!.start()
                        Toast.makeText(context, "Start recording", Toast.LENGTH_SHORT).show()
                        isRec=true
                    }
                }
            } else {
                Snackbar.make(root, "Поле не заполнено", Snackbar.LENGTH_SHORT).show()
            }
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}