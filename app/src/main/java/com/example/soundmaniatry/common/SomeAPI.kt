package com.example.soundmaniatry.common


import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface SomeAPI {
    @GET("files")
    fun getList(@Query("filter") filter : String) : Call<List<String>>

    @GET("load")
    fun getFile(@Query("title") title : String) : Call<ResponseBody>

    @Multipart
    @POST("save")
    fun saveFile(@Query("title") title : String, @PartMap params: HashMap<String, RequestBody>?) : Call<ResponseBody>

    @DELETE("delete")
    fun deleteFile(@Query("title") title : String) : Call<ResponseBody>
}