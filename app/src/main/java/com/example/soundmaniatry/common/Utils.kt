package com.example.soundmaniatry.common

import android.os.Environment
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File

class Utils {
    fun getFile() : File {
        val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).path + "/.Sndmania/")
        return file
    }

    fun getRetrofit() : SomeAPI {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://192.168.1.142:4000/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(SomeAPI::class.java)
    }
}