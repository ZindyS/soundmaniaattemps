package com.example.soundmaniatry

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.soundmaniatry.ui.HomeFragment
import kotlinx.android.synthetic.main.item.view.*
import java.io.File

class RecyclerAdapter(val list : MutableList<File>, val context : Context) : RecyclerView.Adapter<RecyclerAdapter.VH>() {
    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textV = itemView.textView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return  VH(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.textV.text = list[position].name
        holder.itemView.setOnClickListener {
            HomeFragment.startMusic(list[position], holder.itemView.context)
        }
        holder.itemView.setOnLongClickListener {
            HomeFragment.deleteMusic(list[position], context, holder.itemView)
            return@setOnLongClickListener false
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}